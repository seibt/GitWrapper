# GitWrapper #

A set of classes for working with git repositories.
The repository is examined using a wrapper around native git calls.
The current feature set is limited to extraction of merge commits and associated operations. 

### Setup ###

GitWrapper uses the Gradle build system.

Using `./gradlew build` will assemble a .jar file containing the library in the `build/libs` directory.
The dependencies of the library may be displayed using `./gradlew dependencies --configuration runtime`.

If the project using the library is also a gradle project, it is far more convenient to add the library as a sub-project.
The code below assumes that the directory of your clone of GitWrapper is a sibling of the project directory using it.
Extend your `settings.gradle` and `build.gradle` as follows:

**settings.gradle**
```groovy
includeFlat 'GitWrapper' // The name of the directory containing your clone of GitWrapper.
```

**build.gradle**
```groovy
dependencies {
    compile project(':GitWrapper')
}
```

### Logging ###

GitWrapper classes uses the Java standard logging framework for recording exceptions and status messages.
The file `GitWrapperLogging.properties` contains an example configuration in which console output for logging messages of the WARNING level is configured for all loggers.
All loggers have the canonical name of the class they are used in and are therefore sub-loggers of `de.uni_passau.fim.seibt.gitwrapper`.

One way of configuring logging is to pass `-Djava.util.logging.config.file=<PATH TO Logging.properties>` to the JVM that is using GitWrapper.
For other options see [the documentation](https://docs.oracle.com/en/java/javase/11/docs/api/java.logging/java/util/logging/package-summary.html),
specifically that of [LogManager](https://docs.oracle.com/en/java/javase/11/docs/api/java.logging/java/util/logging/LogManager.html).
For a more general overview see [this link](https://docs.oracle.com/en/java/javase/11/core/java-logging-overview.html).

In general, WARNING will contain all messages indicating that something went wrong, FINE and FINER will contain (more verbose) status messages and FINEST will print all output of any external commands being called by the library.

### Example ###

The following example code will clone a given repository to the current working directory and display the SHA1 hashes of all merge commits.

```java
GitWrapper git;

try {
    git = new GitWrapper("git"); // Or /usr/bin/git, C:\Program Files\Git\bin\git.
} catch (ToolNotWorkingException ex) {
    // Handle the case that git can not be called using the supplied command.
    return;
}

Optional<Repository> optRepo = git.clone(new File("."), "git@github.com:se-sic/jdime.git", false);

optRepo.ifPresent(repo -> repo.getMergeCommits().forEach(mergeCommit -> {
    System.out.println("Merge Commit: " + mergeCommit.getId());
}));
```

### Contact ###
* Georg Seibt (seibt[at]fim.uni-passau.de)
