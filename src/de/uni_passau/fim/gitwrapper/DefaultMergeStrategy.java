/**
 * Copyright (C) 2022 Georg Seibt
 *
 * This file is part of GitWrapper.
 *
 * GitWrapper is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GitWrapper is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with GitWrapper. If not, see <http://www.gnu.org/licenses/>.
 */
package de.uni_passau.fim.gitwrapper;

import de.uni_passau.fim.processexecutor.ProcessExecutor;

import java.util.Optional;

/**
 * A {@link MergeStrategy} representing the default 'git merge' merge implementation.
 */
public class DefaultMergeStrategy implements MergeStrategy {

    private final Repository repo;

    /**
     * The default merge strategy that git uses when no
     * <a href="https://git-scm.com/docs/git-merge#_merge_strategies">git merge</a> strategy is specified.
     *
     * @param repo
     *         the repo the strategy operates on
     */
    public DefaultMergeStrategy(Repository repo) {
        this.repo = repo;
    }

    @Override
    public Optional<Integer> merge(Reference left, Reference right) {
        Optional<ProcessExecutor.ExecRes> mergeBase = repo.getGit().exec(repo.getDir(), "merge", "-n", "-q", right.getId());
        return mergeBase.flatMap(res -> repo.getStatus().map(stat -> stat.unmerged.size()));
    }

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }
}
