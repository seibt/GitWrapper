/**
 * Copyright (C) 2022 Georg Seibt
 *
 * This file is part of GitWrapper.
 *
 * GitWrapper is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GitWrapper is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with GitWrapper. If not, see <http://www.gnu.org/licenses/>.
 */
package de.uni_passau.fim.gitwrapper;

import java.time.OffsetDateTime;
import java.util.logging.Logger;

/**
 * Represents the dummy commit that is used by git to represent uncommitted changes. Every {@link Repository}
 * constructs its {@link DummyCommit} in its constructor.
 */
final class DummyCommit extends Commit {

    private static final Logger LOG = Logger.getLogger(DummyCommit.class.getCanonicalName());

    /**
     * This commit ID is used for the dummy commit representing uncommitted changes.
     */
    public static final String DUMMY_COMMIT_ID = "0000000000000000000000000000000000000000";

    /**
     * Constructs the dummy {@link Commit} with ID {@value DUMMY_COMMIT_ID} for the given {@link Repository}.
     *
     * @param repo
     *         the {@link Repository} for which this {@link Commit} represents uncommitted changes
     */
    DummyCommit(Repository repo) {
        super(repo, DUMMY_COMMIT_ID);
        super.setAuthor("Not Committed Yet");
        super.setAuthorMail("not.committed.yet");
        super.setAuthorTime(OffsetDateTime.now());
        super.setCommitter("Not Committed Yet");
        super.setCommitterMail("not.committed.yet");
        super.setCommitterTime(OffsetDateTime.now());
    }

    @Override
    void setAuthor(String author) {
        setterWarning();
    }

    @Override
    void setAuthorMail(String authorMail) {
        setterWarning();
    }

    @Override
    void setAuthorTime(OffsetDateTime authorTime) {
        setterWarning();
    }

    @Override
    void setCommitter(String committer) {
        setterWarning();
    }

    @Override
    void setCommitterMail(String committerMail) {
        setterWarning();
    }

    @Override
    void setCommitterTime(OffsetDateTime committerTime) {
        setterWarning();
    }

    private void setterWarning() {
        LOG.finest(() -> "Ignoring a setter call on the DummyCommit for " + repo);
        // TODO gets called often while parsing blame lines
    }
}
