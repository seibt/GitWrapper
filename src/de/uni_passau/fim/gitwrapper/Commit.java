/**
 * Copyright (C) 2022 Georg Seibt
 *
 * This file is part of GitWrapper.
 *
 * GitWrapper is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GitWrapper is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with GitWrapper. If not, see <http://www.gnu.org/licenses/>.
 */
package de.uni_passau.fim.gitwrapper;

import de.uni_passau.fim.processexecutor.ProcessExecutor;

import java.nio.file.Path;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A {@link Commit} made in a {@link Repository}.
 */
public class Commit extends Reference {

    private static final Logger LOG = Logger.getLogger(Commit.class.getCanonicalName());

    // Regexes for parsing the 'git cat-file -p' output for a commit object. These might depend on the git version.
    private static final Pattern TREE_INFO = Pattern.compile("tree (.{40})\\nparent (.{40})\\n");
    private static final Pattern AUTHOR_INFO = Pattern.compile("author (.*?)<(.*?)> (\\d+) ([+-]\\d{4})\\n");
    private static final Pattern COMMITTER_INFO = Pattern.compile("committer (.*?)<(.*?)> (\\d+) ([+-]\\d{4})\\n");
    private static final Pattern EMPTY_LINE = Pattern.compile("^\\s*$", Pattern.MULTILINE);

    private String message;

    private String author;
    private String authorMail;
    private OffsetDateTime authorTime;

    private String committer;
    private String committerMail;
    private OffsetDateTime committerTime;

    /**
     * Constructs a new {@link Commit} with the given <code>id</code> made in the <code>repo</code>.
     *
     * @param repo
     *         the {@link Repository} the {@link Commit} was made in
     * @param id
     *         the ID of the {@link Commit}
     */
    Commit(Repository repo, String id) {
        super(repo, id);
    }

    /**
     * Checks if the given {@link Commit} is an ancestor of this commit.
     *
     * @param ancestor
     *         the {@link Commit} to check
     * @return optionally <code>true</code>, if the given commit is in the history before this commit,
     * <code>false</code>, if not and an {@link Optional#EMPTY empty optional}, if an error occurred.
     */
    public Optional<Boolean> checkAncestry(Commit ancestor) {
        Optional<ProcessExecutor.ExecRes> res = git.exec(repo.getDir(), "merge-base", "--is-ancestor", ancestor.getId(), id);
        Function<ProcessExecutor.ExecRes, Boolean> toBoolean = execRes -> {

            if (execRes.exitCode == 0) {
                return Boolean.TRUE;
            }

            if (execRes.exitCode == 1) {
                return Boolean.FALSE;
            }

            LOG.warning(() -> "Failed to determine whether '" + ancestor + "' is an ancestor of '" + this + "'.");
            return null;
        };

        return res.map(toBoolean);
    }

    /**
     * Returns the paths that were affected by this {@link Commit}.
     *
     * @return optionally the {@link List} of {@link Path Paths}
     */
    public Optional<List<Path>> getAffectedPaths() {
        Optional<ProcessExecutor.ExecRes> res = git.exec(repo.getDir(), "log", "-m", "-1", "--name-only", "--pretty=format:", id);
        Function<ProcessExecutor.ExecRes, List<Path>> toPaths = execRes -> {
            Supplier<String> errorMsg = () -> "Failed to determine the paths affected by '" + this + "'.";

            if (git.failed(execRes)) {
                LOG.warning(errorMsg);
                return null;
            }

            String[] lines = execRes.getStdOutTrimmed().split("\\R");
            List<Path> paths = new ArrayList<>();

            for (String line : lines) {
                Optional<Path> oPath = git.getPath(line);

                if (oPath.isPresent()) {
                    paths.add(oPath.get());
                } else {
                    LOG.warning(errorMsg);
                    return null;
                }
            }

            return paths;
        };

        return res.map(toPaths);
    }

    /**
     * Returns the author of this commit.
     *
     * @return the author
     */
    public String getAuthor() {
        if (author == null) {
            getCommitInfo();
        }

        return author;
    }

    /**
     * Sets the author to the new value.
     *
     * @param author
     *         the new author
     */
    void setAuthor(String author) {
        this.author = author;
    }

    /**
     * Returns the author mail of this commit.
     *
     * @return the author mail
     */
    public String getAuthorMail() {
        if (authorMail == null) {
            getCommitInfo();
        }

        return authorMail;
    }

    /**
     * Sets the author mail to the new value.
     *
     * @param authorMail
     *         the new author mail
     */
    void setAuthorMail(String authorMail) {
        this.authorMail = authorMail;
    }

    /**
     * Returns the author time of this commit.
     *
     * @return the author time
     */
    public OffsetDateTime getAuthorTime() {
        if (authorTime == null) {
            getCommitInfo();
        }

        return authorTime;
    }

    /**
     * Sets the author time to the new value.
     *
     * @param authorTime
     *         the new author time
     */
    void setAuthorTime(OffsetDateTime authorTime) {
        this.authorTime = authorTime;
    }

    /**
     * Returns the committer.
     *
     * @return the committer
     */
    public String getCommitter() {
        if (committer == null) {
            getCommitInfo();
        }

        return committer;
    }

    /**
     * Sets the committer to the new value.
     *
     * @param committer
     *         the new committer
     */
    void setCommitter(String committer) {
        this.committer = committer;
    }

    /**
     * Returns the committer mail.
     *
     * @return the committer mail
     */
    public String getCommitterMail() {
        if (committerMail == null) {
            getCommitInfo();
        }

        return committerMail;
    }

    /**
     * Sets the committer mail to the new value.
     *
     * @param committerMail
     *         the new committer mail
     */
    void setCommitterMail(String committerMail) {
        this.committerMail = committerMail;
    }

    /**
     * Returns the committer time.
     *
     * @return the committer time
     */
    public OffsetDateTime getCommitterTime() {
        if (committerTime == null) {
            getCommitInfo();
        }

        return committerTime;
    }

    /**
     * Sets the committer time to the new value.
     *
     * @param committerTime
     *         the new committer time
     */
    void setCommitterTime(OffsetDateTime committerTime) {
        this.committerTime = committerTime;
    }

    /**
     * Returns the commit message.
     *
     * @return the commit message
     */
    public String getMessage() {
        if (message == null) {
            getCommitInfo();
        }

        return message;
    }

    /**
     * Sets the commit message.
     *
     * @param message
     *         the commit message
     */
    void setMessage(String message) {
        this.message = message;
    }

    /**
     * Initializes all uninitialized fields of this {@link Commit} from its 'git cat-file -p <id>' output.
     */
    private void getCommitInfo() {
        git.exec(repo.getDir(), "cat-file", "-p", id).ifPresent(execRes -> {
            if (git.failed(execRes)) {
                LOG.warning(() -> String.format("Failed to obtain information about commit %s.", this));
                return;
            }

            String result = execRes.stdOut;
            Matcher matcher;

            if (author != null && authorMail != null && authorTime != null) {
                LOG.finer("Skipping initialization author information. Already initialized.");
            } else if (!(matcher = AUTHOR_INFO.matcher(result)).find()) {
                LOG.warning(() -> String.format("Unexpected output while getting author info for %s.", this));
            } else {
                author = matcher.group(1);
                authorMail = matcher.group(2);
                authorTime = OffsetDateTime.ofInstant(Instant.ofEpochSecond(Long.parseLong(matcher.group(3))), ZoneId.of(matcher.group(4)));
            }

            if (committer != null && committerMail != null && committerTime != null) {
                LOG.finer("Skipping initialization committer information. Already initialized.");
            } else if (!(matcher = COMMITTER_INFO.matcher(result)).find()) {
                LOG.warning(() -> String.format("Unexpected output while getting committer info for %s.", this));
            } else {
                committer = matcher.group(1);
                committerMail = matcher.group(2);
                committerTime = OffsetDateTime.ofInstant(Instant.ofEpochSecond(Long.parseLong(matcher.group(3))), ZoneId.of(matcher.group(4)));
            }

            if (message != null) {
                LOG.finer("Skipping initialization of commit message. Already initialized.");
            } else if (!(matcher = EMPTY_LINE.matcher(result)).find()) {
                LOG.warning(() -> String.format("Unexpected output while getting the message for %s.", this));
            } else {
                message = result.substring(matcher.end()).trim();
            }
        });
    }
}
