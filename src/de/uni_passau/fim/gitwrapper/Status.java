/**
 * Copyright (C) 2022 Georg Seibt
 *
 * This file is part of GitWrapper.
 *
 * GitWrapper is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GitWrapper is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with GitWrapper. If not, see <http://www.gnu.org/licenses/>.
 */
package de.uni_passau.fim.gitwrapper;

import java.nio.file.Path;
import java.util.*;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static de.uni_passau.fim.gitwrapper.Status.StatusCode.*;

/**
 * The status of a {@link Repository}.
 *
 * @see Repository#getStatus()
 */
public class Status {

    private static final Logger LOG = Logger.getLogger(Status.class.getCanonicalName());

    private static final String G_RCODE = "rcode";
    private static final String G_TO = "to";
    private static final String G_FROM = "from";
    private static final String G_CODE = "code";
    private static final String G_PATH = "path";

    private static final String regex = String.format("(?:(?<%s>R[ MD]) (?<%s>[^\0]*)\0(?<%s>[^\0]*)\0|(?<%s>[ MADRCU]{2}|\\?\\?|!!) (?<%s>[^\0]*)\0)",
                                                      G_RCODE, G_TO, G_FROM, G_CODE, G_PATH);
    private static final Pattern STATUS_ENTRY = Pattern.compile(regex);

    /**
     * Two {@link StatusCode StatusCodes} are used to represent the status of a file in a git {@link Repository}.
     *
     * @see <a href=https://git-scm.com/docs/git-status>The Documentation of 'git status'</a>
     */
    public enum StatusCode {
        UNMODIFIED,
        MODIFIED,
        ADDED,
        DELETED,
        RENAMED,
        COPIED,

        /**
         * Updated but unmerged.
         */
        UNMERGED,
        UNTRACKED,
        IGNORED;

        /**
         * Returns the enum constant representing the given status code character. Must be one of ' ', 'M', 'A', 'D',
         * 'R', 'C', 'U', '?', '!'.
         *
         * @param c
         *         the status character
         * @return the corresponding enum constant
         * @throws IllegalArgumentException
         *         if there is no enum constant for the given character
         * @see <a href=https://git-scm.com/docs/git-status>The Documentation of 'git status'</a>
         */
        private static StatusCode forChar(char c) {

            switch (c) {
                case ' ':
                    return UNMODIFIED;
                case 'M':
                    return MODIFIED;
                case 'A':
                    return ADDED;
                case 'D':
                    return DELETED;
                case 'R':
                    return RENAMED;
                case 'C':
                    return COPIED;
                case 'U':
                    return UNMERGED;
                case '?':
                    return UNTRACKED;
                case '!':
                    return IGNORED;
                default:
                    throw new IllegalArgumentException("No enum constant for status code '" + c + "'.");
            }
        }
    }

    /**
     * Represents the status of a file in a git {@link Repository}. For files with merge conflicts, {@link #x} and
     * {@link #y} show the modification states of each side of the merge. For files that do not have merge conflicts,
     * {@link #x} shows the status of the index, and {@link #y} shows the status of the work tree.
     */
    public static class FileStatus {

        /**
         * The first status code.
         */
        public final StatusCode x;

        /**
         * The second status code.
         */
        public final StatusCode y;

        /**
         * In case of renamings this path represents the filename before the renaming.
         */
        public final Optional<Path> oldFile;

        /**
         * The file whose status is represented by this {@link FileStatus} instance.
         */
        public final Path file;

        private FileStatus(StatusCode x, StatusCode y, Optional<Path> oldFile, Path file) {

            if ((x == UNTRACKED ^ y == UNTRACKED) || (x == IGNORED ^ y == IGNORED)) {
                throw new IllegalArgumentException("If one of the two status codes is UNTRACKED or IGNORED, the other must be the same.");
            }

            this.x = x;
            this.y = y;
            this.oldFile = oldFile;
            this.file = file;
        }
    }

    /**
     * The commit, this {@link Status} is based on.
     */
    public final Commit commit;

    /**
     * A map of files which were changed since the last {@link #commit} and their status codes.
     */
    public final Map<Path, FileStatus> changed;

    /**
     * A map of files which are in an unmerged state and their status codes.
     */
    public final Map<Path, FileStatus> unmerged;

    /**
     * The list of untracked files.
     */
    public final List<Path> untracked;

    /**
     * The list of ignored files.
     */
    public final List<Path> ignored;

    /**
     * Constructs a new {@link Status}.
     *
     * @param commit
     *         the current HEAD {@link Commit} at the time this {@link Status} is constructed
     * @param changed
     *         the map of changed files and heir status codes
     * @param unmerged
     *         the map of unmerged files an their status code
     * @param untracked
     *         the list of untracked files
     * @param ignored
     *         the list of ignored files
     */
    private Status(Commit commit, Map<Path, FileStatus> changed, Map<Path, FileStatus> unmerged, List<Path> untracked, List<Path> ignored) {
        this.commit = commit;
        this.changed = changed.isEmpty() ? Collections.emptyMap() : Collections.unmodifiableMap(changed);
        this.unmerged = unmerged.isEmpty() ? Collections.emptyMap() : Collections.unmodifiableMap(unmerged);
        this.untracked = untracked.isEmpty() ? Collections.emptyList() : Collections.unmodifiableList(untracked);
        this.ignored = ignored.isEmpty() ? Collections.emptyList() : Collections.unmodifiableList(ignored);
    }

    /**
     * Returns whether this {@link Status} represents a clean repository.
     *
     * @return <code>true</code>, if the repository state is clean and has no uncommitted changes.
     */
    public boolean isClean() {
        return changed.isEmpty() && unmerged.isEmpty();
    }

    /**
     * Returns whether this {@link Status} has unmerged files.
     *
     * @return <code>true</code>, if there are unmerged files
     */
    public boolean hasConflicts() {
        return !unmerged.isEmpty();
    }

    @Override
    public String toString() {
        String out = "";
        if (isClean()) {
            out = "Clean working directory on commit " + commit + "\n";
        } else {
            if (!unmerged.isEmpty()) {
                out = "Unmerged files:";
                out += unmerged.keySet().stream().map(Path::toString).reduce("", (list, file) -> String.join("\n", list, file));
                out += "\n";
            }
            if (!changed.isEmpty()) {
                out += "Changed files:";
                out += changed.keySet().stream().map(Path::toString).reduce("", (list, file) -> String.join("\n", list, file));
                out += "\n";
            }
        }

        return out;
    }

    /**
     * Parses the output from git into a new object.
     *
     * @param repo
     *         the repo this status report is from
     * @param gitOutput
     *         the status report from git
     * @return optionally a {@link Status} object, representing the status read from the git output
     */
    static Optional<Status> parseStatus(Repository repo, String gitOutput) {
        Map<Path, FileStatus> changed = new LinkedHashMap<>();
        Map<Path, FileStatus> unmerged = new LinkedHashMap<>();
        List<Path> untracked = new ArrayList<>();
        List<Path> ignored = new ArrayList<>();

        GitWrapper git = repo.getGit();
        Matcher matcher = STATUS_ENTRY.matcher(gitOutput);

        while (matcher.find()) {
            String code;
            StatusCode x;
            StatusCode y;

            Path file;
            Optional<Path> oldFile;

            if ((code = matcher.group(G_CODE)) != null) {
                Optional<Path> oPath = git.getPath(matcher.group(G_PATH));

                if (oPath.isPresent()) {
                    file = oPath.get();
                    oldFile = Optional.empty();
                } else {
                    LOG.warning(() -> "Failed to parse the file path for status entry '" + matcher.group() + "'.");
                    continue;
                }
            } else if ((code = matcher.group(G_RCODE)) != null) {
                Optional<Path> oToPath = git.getPath(matcher.group(G_TO));
                Optional<Path> oFromPath = git.getPath(matcher.group(G_FROM));

                if (oToPath.isPresent() && oFromPath.isPresent()) {
                    file = oToPath.get();
                    oldFile = oFromPath;
                } else {
                    LOG.warning(() -> "Failed to parse a file path for status entry '" + matcher.group() + "'.");
                    continue;
                }
            } else {
                // This 'should' not be possible...
                LOG.warning(() -> "Could not find the status codes for the status entry '" + matcher.group() + "'.");
                continue;
            }

            x = StatusCode.forChar(code.charAt(0));
            y = StatusCode.forChar(code.charAt(1));

            if (x == UNTRACKED && y == UNTRACKED) {
                untracked.add(file);
            } else if (x == IGNORED && y == IGNORED) {
                ignored.add(file);
            } else {
                FileStatus status = new FileStatus(x, y, oldFile, file);

                if (x == UNMERGED || y == UNMERGED || (x == ADDED && y == ADDED) || (x == DELETED && y == DELETED)) {
                    unmerged.put(file, status);
                } else {
                    changed.put(file, status);
                }
            }
        }

        return repo.getCurrentHEAD().map(head -> new Status(head, changed, unmerged, untracked, ignored));
    }
}
