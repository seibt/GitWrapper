/**
 * Copyright (C) 2022 Georg Seibt
 *
 * This file is part of GitWrapper.
 *
 * GitWrapper is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GitWrapper is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with GitWrapper. If not, see <http://www.gnu.org/licenses/>.
 */
package de.uni_passau.fim.gitwrapper;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static de.uni_passau.fim.gitwrapper.Repository.ConflictMarker.*;
import static de.uni_passau.fim.processexecutor.ProcessExecutor.ExecRes;
import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * A git {@link Repository}.
 */
public class Repository {

    private static final Logger LOG = Logger.getLogger(Repository.class.getCanonicalName());

    // Constants defining expected commit header fields when parsing the output of 'git blame --porcelain'.
    private static final String AUTHOR = "author";
    private static final String AUTHOR_MAIL = "author-mail";
    private static final String AUTHOR_TIME = "author-time";
    private static final String AUTHOR_TZ = "author-tz";
    private static final String COMMITTER = "committer";
    private static final String COMMITTER_MAIL = "committer-mail";
    private static final String COMMITTER_TIME = "committer-time";
    private static final String COMMITTER_TZ = "committer-tz";
    private static final String FILENAME = "filename";
    private static final String SUMMARY = "summary";
    private static final String BOUNDARY = "boundary";

    // The type returned for commits by 'git cat-file -t".
    private static final String TYPE_COMMIT = "commit";

    /**
     * An enumeration of the three possible conflict markers in an unmerged file.
     */
    enum ConflictMarker {
        CONFLICT_START("<<<<<<<"),
        CONFLICT_MIDDLE("======="),
        CONFLICT_END(">>>>>>>");

        /**
         * The expected characters at the beginning of a line representing a
         * conflict marker.
         */
        public final String prefix;

        ConflictMarker(String prefix) {
            this.prefix = prefix;
        }
    }

    private GitWrapper git;

    private String url;
    private File dir;

    private Map<String, Commit> commits;
    private Map<String, Branch> branches;

    /**
     * Constructs a new {@link Repository}.
     *
     * @param git
     *         the {@link GitWrapper} to use
     * @param url
     *         the url the {@link Repository} was cloned from
     * @param dir
     *         the directory containing the {@link Repository}
     */
    Repository(GitWrapper git, String url, File dir) {
        this.git = git;
        this.url = url;

        try {
            this.dir = dir.getCanonicalFile();
        } catch (IOException e) {
            LOG.log(Level.WARNING, e, () -> "Exception computing the canonical file for " + dir + ". Using the absolute file instead.");
            this.dir = dir.getAbsoluteFile();
        }

        this.commits = new HashMap<>();
        this.commits.put(DummyCommit.DUMMY_COMMIT_ID, new DummyCommit(this));
        this.branches = new HashMap<>();
    }

    /**
     * Performs a checkout of the given {@link Reference}.
     *
     * @param ref
     *         the {@link Reference} to checkout
     * @return whether the checkout was successful
     * @see <a href=https://git-scm.com/docs/git-checkout>git checkout</a>
     */
    public boolean checkout(Reference ref) {
        Optional<ExecRes> checkout = git.exec(dir, "checkout", ref.getId());
        Function<ExecRes, Boolean> toBoolean = res -> {
            boolean failed = git.failed(res);

            if (failed) {
                LOG.warning(() -> String.format("Checkout of %s failed.", ref));
            }

            return !failed;
        };

        return checkout.map(toBoolean).map(co -> co && syncSubmodules()).orElse(false);
    }

    /**
     * Performs a checkout of the given {@link Reference}. All changes since last commit will be discarded.
     *
     * @param ref
     *         the {@link Reference} to checkout
     * @return whether the checkout was successful
     * @see <a href=https://git-scm.com/docs/git-checkout>git checkout</a>
     */
    public boolean forceCheckout(Reference ref) {
        Optional<ExecRes> checkout = git.exec(dir, "checkout", "-f", ref.getId());
        Function<ExecRes, Boolean> toBoolean = res -> {
            boolean failed = git.failed(res);

            if (failed) {
                LOG.warning(() -> String.format("Reset to %s failed.", ref));
                return false;
            }

            return true;
        };

        return checkout.map(toBoolean).map(co -> co && syncSubmodules()).orElse(false);
    }

    /**
     * Synchronizes and initializes the submodules of this repository.
     *
     * @return whether the synchronization was successful
     */
    private Boolean syncSubmodules() {
        Optional<ExecRes> init = git.exec(dir, "submodule", "init");
        Optional<ExecRes> sync = init.filter(ExecRes::succeeded)
                                     .flatMap(res -> git.exec(dir, "submodule", "sync"));
        Optional<ExecRes> update = sync.filter(ExecRes::succeeded)
                                       .flatMap(res -> git.exec(dir, "submodule", "update", "--init", "--recursive"));

        Function<ExecRes, Boolean> toBoolean = res -> {
            boolean failed = git.failed(res);

            if (failed) {
                LOG.warning(() -> "Failed to sync the submodules of " + Repository.this);
                return false;
            }

            return true;
        };

        return update.map(toBoolean).orElse(false);
    }

    /**
     * Performs a fetch in this {@link Repository}.
     *
     * @return whether the fetch was successful
     * @see <a href=https://git-scm.com/docs/git-fetch>git fetch</a>
     */
    public boolean fetch() {
        Optional<ExecRes> fetch = git.exec(dir, "fetch", "--all", "--tags"); //TODO add "--prune"?
        Function<ExecRes, Boolean> toBoolean = res -> {
            boolean failed = git.failed(res);

            if (failed) {
                LOG.warning(() -> String.format("Fetch of %s failed.", this));
            }

            return !failed;
        };

        return fetch.map(toBoolean).orElse(false);
    }

    /**
     * Optionally returns a list of all merge commits in this repository. An empty {@link Optional} will be returned
     * if there is an error retrieving the merge commits.
     *
     * @return the list of merge commits
     */
    public Optional<List<Commit>> getMergeCommits() {
        Optional<ExecRes> revList = git.exec(dir, "rev-list", "--all", "--merges");
        Function<ExecRes, List<Commit>> toCommitList = res -> {

            if (git.failed(res)) {
                LOG.warning(() -> String.format("Failed to obtain the merge commits from %s.", this));
                return null;
            }

            String[] lines;

            if (res.getStdOutTrimmed().isEmpty()) {
                lines = new String[] {};
            } else {
                lines = res.getStdOutTrimmed().split("[\\r?\\n]+");
            }

            LOG.fine(() -> String.format("Found %d merge commits in %s.", lines.length, this));

            if (lines.length > 0) {
                LOG.finer(() -> String.format("Merge commits are:%n%s", String.join(System.lineSeparator(), lines)));
            }

            return Arrays.stream(lines).map(this::getCommitUnchecked).collect(Collectors.toList());
        };

        return revList.map(toCommitList);
    }

    /**
     * Returns the {@link Commit} currently pointed at by <code>HEAD</code>.
     *
     * @return the <code>HEAD</code> commit or an empty {@link Optional} if there was an error
     */
    public Optional<Commit> getCurrentHEAD() {
        Optional<ExecRes> revParse = git.exec(dir, "rev-parse", "HEAD");
        Function<ExecRes, Commit> toHEAD = res -> {

            if (git.failed(res)) {
                LOG.warning(() -> "Failed to obtain the current HEAD commit.");
                return null;
            }

            return getCommitUnchecked(res.getStdOutTrimmed());
        };

        return revParse.map(toHEAD);
    }

    /**
     * Returns a {@link Commit} for the given ID. The caller must ensure that the ID is a full SHA1 hash of a
     * commit that exists in this repository.
     *
     * @param id
     *         the ID for the {@link Commit}
     * @return the {@link Commit}
     * @see #getCommit(String)
     */
    Commit getCommitUnchecked(String id) {
        return commits.computeIfAbsent(id, theID -> new Commit(this, theID));
    }

    /**
     * Optionally returns a {@link Commit} for the given ID. If the given ID does not designate a commit that exists in
     * this {@link Repository}, an empty {@link Optional} will be returned. The ID will be resolved to a full SHA1 hash.
     *
     * @param id
     *         the ID of the commit
     * @return the {@link Commit} or an empty {@link Optional} if the ID is invalid or an exception occurs
     */
    public Optional<Commit> getCommit(String id) {
        if (commits.containsKey(id)) {
            return Optional.of(commits.get(id));
        }

        return verify(false, id, TYPE_COMMIT).map(sha1 -> commits.computeIfAbsent(sha1, fullID -> new Commit(this, fullID)));
    }

    /**
     * Optionally returns a {@link Branch} for the given name. If the given name does not designate a branch that
     * exists in this {@link Repository}, an empty {@link Optional} will be returned.
     *
     * @param name
     *         the name of the branch
     * @return the {@link Branch} or an empty {@link Optional} if the name is invalid or an exception occurs
     */
    public Optional<Branch> getBranch(String name) {
        if (branches.containsKey(name)) {
            return Optional.of(branches.get(name));
        }

        Function<String, Branch> toBranch = fullName -> {
            if (fullName.isEmpty()) {
                LOG.warning(() -> String.format("The name '%s' does not designate a branch in %s.", name, this));
                return null;
            }

            boolean remote;

            if (fullName.startsWith(Branch.FULL_SYMBOLIC_BRANCH)) {
                remote = false;
            } else if (fullName.startsWith(Branch.FULL_SYMBOLIC_REMOTE_BRANCH)) {
                remote = true;
            } else {
                LOG.warning(() -> String.format("The name '%s' designates neither a local nor a remote branch in %s.", fullName, this));
                return null;
            }

            return branches.computeIfAbsent(fullName, newBranchName -> new Branch(this, newBranchName, remote));
        };

        return verify(true, name, null).map(toBranch);
    }

    /**
     * Returns the {@link Branch branches} present in this {@link Repository}.
     *
     * @param remote whether to return remote branches only, otherwise only local branches are returned
     * @return the branches or an empty {@link Optional} if an exception occurs
     */
    public Optional<List<Branch>> getBranches(boolean remote) {
        Optional<ExecRes> optBranchListing;

        if (remote) {
            optBranchListing = git.exec(dir, "branch", "--remotes ", "--list", "--format=%(refname)");
        } else {
            optBranchListing = git.exec(dir, "branch",               "--list", "--format=%(refname)");
        }

        return optBranchListing.map(res -> {
            if (git.failed(res)) {
                LOG.warning("Failed to get the branch names.");
                return null;
            }

            List<Optional<Branch>> optBranches = Arrays.stream(res.getStdOutTrimmed().split("\\R"))
                                                        // This list includes '(HEAD detached at <HASH>)' in detached HEAD state
                                                        .filter(line -> line.startsWith(Branch.FULL_SYMBOLIC_BRANCH) ||
                                                                        line.startsWith(Branch.FULL_SYMBOLIC_REMOTE_BRANCH))
                                                        .map(this::getBranch).collect(Collectors.toList());

            if (optBranches.stream().anyMatch(Optional::isEmpty)) {
                LOG.warning("Failed to get all branches.");
                return null;
            }

            return optBranches.stream().flatMap(Optional::stream).collect(Collectors.toList());
        });
    }

    /**
     * Resolves the given <code>id</code> to the full SHA1 hash.
     *
     * @param id
     *         the <code>id</code> to transform
     * @return the full SHA1 hash or an empty {@link Optional} if an exception occurs or {@code id} can not be converted
     *         to its associated git hash
     */
    protected Optional<String> toHash(String id) {
        return verify(false, id, null);
    }

    /**
     * Verifies that the given git-thing exists and can be resolved to a raw SHA1 hash. If type is not {@link null},
     * git will be asked to verify that the thing is also of the given type. If {@code fullSymbolicName} is set, the
     * returned {@code String} will be (if present) the full symbolic name of the reference. If {@code arg} does
     * not designate a reference but does exist in the repository, an empty {@code String} will be returned. This
     * is the case (for example) for an existing commit in the repository.
     *
     * @param fullSymbolicName
     *         whether to return the full symbolic name of {@code arg} if possible
     * @param arg
     *         the thing to check
     * @param type
     *         the type of the thing, ignored if {@code null}
     * @return optionally the full SHA1 hash (or full symbolic name) or an empty {@link Optional} if the thing can not
     *         be found or turned into a full hash
     */
    private Optional<String> verify(boolean fullSymbolicName, String arg, String type) {
        String argument = type != null ? String.format("%s^{%s}", arg, type) : arg;
        Optional<ExecRes> revParse;

        if (fullSymbolicName) {
            revParse = git.exec(dir, "rev-parse", "--symbolic-full-name", "--verify", argument);
        } else {
            revParse = git.exec(dir, "rev-parse", "--verify", argument);
        }

        Function<ExecRes, String> toHash = res -> {

            if (git.failed(res)) {
                LOG.warning(() -> String.format("Failed to resolve '%s' to its unique SHA1 hash.", arg));
                return null;
            }

            String hash = res.getStdOutTrimmed();
            LOG.finer(() -> String.format("Verified that %s exists and resolved it to %s.", arg, hash));

            return hash;
        };

        return revParse.map(toHash);
    }

    /**
     * Copies this {@link Repository} to the given <code>destination</code> directory.
     *
     * @param destination
     *         the destination directory
     * @return a copy of this {@link Repository} at the new location
     * @see FileUtils#copyDirectory(File, File)
     */
    public Optional<Repository> copy(File destination) {
        try {
            if (destination.exists() && destination.isDirectory()) {
                LOG.warning(() -> String.format("%s already exists. Merging source and destination directories.", destination));
            }

            FileUtils.copyDirectory(dir, destination);

            LOG.fine(() -> String.format("Copied %s to %s.", this, destination));
        } catch (IOException e) {
            LOG.log(Level.WARNING, e, () -> String.format("Failed to copy %s to %s.", dir, destination));
            return Optional.empty();
        }

        Repository repo = new Repository(git, url, destination);

        for (Map.Entry<String, Commit> entry : commits.entrySet()) {
            repo.getCommitUnchecked(entry.getValue().getId());
        }

        return Optional.of(repo);
    }

    /**
     * Parses the output of 'git blame' for the given {@code file} and returns a list of {@link BlameLine BlameLines}
     * containing the resulting information about every line of the file. If there is an exception parsing or obtaining
     * the 'git blame' output, an empty {@link Optional} will be returned.
     *
     * @param file
     *         the path to the file to analyze (relative to the git root or absolute)
     * @return optionally the list of {@link BlameLine BlameLines}
     */
    public Optional<List<BlameLine>> blameFile(Path file) {
        Path path = dir.toPath().relativize(dir.toPath().resolve(file));
        Optional<ExecRes> output = git.exec(dir, "blame", "-p", path.toString());
        Function<ExecRes, List<BlameLine>> toList = execRes -> {

            if (git.failed(execRes)) {
                LOG.warning(() -> String.format("Failed to get blame information for file %s", path));
                return null;
            }

            List<BlameLine> bLines = null;

            try {
                bLines = parseBlameLines(execRes.stdOut);
            } catch (NoSuchElementException e) {
                LOG.log(Level.WARNING, e, () -> String.format("Failed to parse the blame information for file %s", path));
            }

            return bLines;
        };

        return output.map(toList);
    }

    /**
     * Parses the given 'git blame' output and returns a list of {@link BlameLine BlameLines} containing the resulting
     * information about every line of the output.
     *
     * @param blameOutput
     *         the 'git blame' output to parse
     * @return the list of {@link BlameLine BlameLines}
     */
    private List<BlameLine> parseBlameLines(String blameOutput) {
        Scanner lines = new Scanner(blameOutput);

        if (!lines.hasNextLine()) {
            return Collections.emptyList();
        }

        List<BlameLine> bLines = new ArrayList<>();
        Map<Commit, BlameLine> firstOccurrences = new HashMap<>();

        blameLineLoop:
        while (lines.hasNextLine()) {
            String line;
            Scanner lineScanner;

            // Read the first line of the commit header and construct/get the Commit object.
            line = lines.nextLine();
            lineScanner = new Scanner(line);

            String sha1 = lineScanner.next();
            int originalLineNumber = lineScanner.nextInt();
            int finalLineNumber = lineScanner.nextInt();

            Commit commit = getCommitUnchecked(sha1);

            Instant authorInstant = null;
            Instant committerInstant = null;
            ZoneId authorTZ = null;
            ZoneId committerTZ = null;

            Path commitFile = null;

            Map<String, String> other = new HashMap<>();

            // Read the rest of the commit header and input all fields into the Commit object or the BlameLine fields.
            while (!(line = lines.nextLine()).startsWith("\t")) {
                lineScanner = new Scanner(line);
                String headerKey = lineScanner.next();

                switch (headerKey) {
                    case AUTHOR:
                        commit.setAuthor(lineScanner.nextLine().trim());
                        break;
                    case AUTHOR_MAIL: {
                        String email = lineScanner.nextLine().trim();
                        commit.setAuthorMail(email.substring(1, email.length() - 1));
                    } break;
                    case AUTHOR_TIME:
                        authorInstant = Instant.ofEpochSecond(lineScanner.nextLong());
                        break;
                    case AUTHOR_TZ:
                        authorTZ = ZoneId.of(lineScanner.next());
                        break;
                    case COMMITTER:
                        commit.setCommitter(lineScanner.nextLine().trim());
                        break;
                    case COMMITTER_MAIL: {
                        String email = lineScanner.nextLine().trim();
                        commit.setCommitterMail(email.substring(1, email.length() - 1));
                    } break;
                    case COMMITTER_TIME:
                        committerInstant = Instant.ofEpochSecond(lineScanner.nextLong());
                        break;
                    case COMMITTER_TZ:
                        committerTZ = ZoneId.of(lineScanner.next());
                        break;
                    case FILENAME: {
                        Optional<Path> oPath = git.getPath(lineScanner.nextLine().substring(1));

                        if (oPath.isPresent()) {
                            commitFile = oPath.get();
                        } else {
                            // TODO return Optional.empty() from the blameFile() method?
                            LOG.warning(() -> "Failed to parse the file path for a line in the blame output. Skipping.");
                            skipBlameLine(lines);
                            continue blameLineLoop;
                        }
                    } break;
                    case SUMMARY:
                        // We do not explicitly store the summary of the commit as it is just the first line of its message (for which there is a getter).
                    case BOUNDARY:
                        // We do not explicitly store the information that this commit was the boundary of the 'git blame' call.
                    default:
                        other.put(headerKey, lineScanner.hasNextLine() ? lineScanner.nextLine().trim() : "");
                }
            }

            if (authorInstant != null && authorTZ != null) {
                commit.setAuthorTime(OffsetDateTime.ofInstant(authorInstant, authorTZ));
            }

            if (committerInstant != null && committerTZ != null) {
                commit.setCommitterTime(OffsetDateTime.ofInstant(committerInstant, committerTZ));
            }

            // Read the line starting with a TAB and store it in a BlameLine along with its commit.
            BlameLine bLine;
            line = line.substring(1); // Remove the TAB.

            if (firstOccurrences.containsKey(commit)) {
                bLine = BlameLine.nextOccurrence(firstOccurrences.get(commit), originalLineNumber, finalLineNumber, line);
            } else {
                bLine = BlameLine.firstOccurrence(commit, originalLineNumber, finalLineNumber, commitFile, line, other);
                firstOccurrences.put(commit, bLine);
            }

            bLines.add(bLine);
        }

        return bLines;
    }

    /**
     * Skips all lines returned from the {@code lines} {@link Scanner} up to and including the first one starting
     * with a TAB.
     *
     * @param lines the {@link Scanner} in which to skip lines
     */
    private void skipBlameLine(Scanner lines) {
        while (lines.hasNextLine() && !lines.nextLine().startsWith("\t"));
    }

    /**
     * Parses the output of 'git blame' for an unmerged file and extracts the merge conflicts that occurred in the file.
     * Every merge conflict is represented by a {@link MergeConflict} instance containing the {@link BlameLine BlameLines}
     * that represent the lines involved in the conflict. If there are no conflicts in the given file, an empty list
     * will be returned. If there is an exception parsing or obtaining the 'git blame' output, an empty {@link Optional}
     * will be returned.
     *
     * @param file
     *         the path to the unmerged file to analyze (relative to the git root or absolute)
     * @return optionally the list of {@link MergeConflict MergeConflicts}
     * @see #blameFile(Path)
     */
    public Optional<List<MergeConflict>> blameUnmergedFile(Path file) {
        Optional<List<BlameLine>> blame = blameFile(file);

        if (!blame.isPresent()) {
            return Optional.empty();
        }

        List<MergeConflict> conflicts = new ArrayList<>();

        List<BlameLine> left = null;
        List<BlameLine> right = null;
        ConflictMarker lastMarker = null;

        for (BlameLine line : blame.get()) {
            if (line.line.startsWith(CONFLICT_START.prefix)) {
                left = new ArrayList<>();
                right = new ArrayList<>();
                lastMarker = CONFLICT_START;
            } else if (lastMarker == CONFLICT_START && line.line.startsWith(CONFLICT_MIDDLE.prefix)) {
                lastMarker = CONFLICT_MIDDLE;
            } else if (lastMarker == CONFLICT_MIDDLE && line.line.startsWith(CONFLICT_END.prefix)) {
                conflicts.add(new MergeConflict(left, right));
                lastMarker = CONFLICT_END;
            } else if (CONFLICT_START == lastMarker) {
                left.add(line);
            } else if (CONFLICT_MIDDLE == lastMarker) {
                right.add(line);
            }
        }

        return Optional.of(conflicts);
    }

    /**
     * Parses and returns the current output of 'git status' for this {@link Repository}. This includes untracked files
     * but excludes ignored files. If there is an exception parsing or obtaining the status output, an empty
     * {@link Optional} will be returned.
     *
     * @return optionally the {@link Status} of this {@link Repository}
     */
    public Optional<Status> getStatus() {
        return getStatus(false, true);
    }

    /**
     * Parses and returns the current output of 'git status' for this {@link Repository}.
     * If there is an exception parsing or obtaining the status output, an empty {@link Optional} will be returned.
     *
     * @param ignored
     *         whether to include ignored files in the {@link Status}
     * @param untracked
     *         whether to include untracked files in the {@link Status}
     * @return optionally the {@link Status} of this {@link Repository}
     */
    public Optional<Status> getStatus(boolean ignored, boolean untracked) {
        Optional<ExecRes> output;
        String untrackedPar =  "--untracked=" + (untracked ? "all" : "no");

        if (ignored) {
            output = git.exec(dir, "status", untrackedPar, "--ignored", "-z");
        } else {
            output = git.exec(dir, "status", untrackedPar, "-z");
        }

        Function<ExecRes, Optional<Status>> toStatus = execRes -> {

            if (git.failed(execRes)) {
                LOG.warning(() -> String.format("Failed to get status information for repo %s", this));
                return Optional.empty();
            }

            return Status.parseStatus(this, execRes.stdOut);
        };

        return output.flatMap(toStatus);
    }

    /**
     * Adds a remote to the this repository if it does not yet exist.
     *
     * @param name
     *         the name of the remote
     * @param forkURL
     *         the url of the remote
     * @return <code>true</code>, if adding was successful or the remote already exists, <code>false</code> otherwise
     */
    public boolean addRemote(String name, String forkURL) {
        Optional<ExecRes> checkRes = git.exec(dir, "remote");
        Optional<Boolean> check = checkRes.map(res -> {
                    if (res.failed()) {
                        LOG.warning(String.format("Failed to get list of remotes for %s.", this));
                        return false;
                    }

                    return Arrays.stream(res.getStdOutTrimmed().split("\\R")).anyMatch(remote -> remote.equals(name));
                }
        );

        Function<Boolean, Boolean> addRemote = res -> res || git.exec(dir, "remote", "add", "-f", name, forkURL).map(addRes -> {
                    if (addRes.exitCode != 0) {
                        LOG.warning(String.format("Failed to add remote %s.", name));
                        return false;
                    }
                    return true;
                }
        ).orElse(false);

        return check.map(addRemote).orElse(false);
    }

    /**
     * Creates a clean working directory at the currently checked out Reference
     *
     * @param retain
     *         patterns to be used as <a href=https://git-scm.com/docs/git-clean#git-clean--eltpatterngt>ignore rules</a>
     * @return {@code true} iff the cleanup was successful
     */
    public boolean cleanup(String... retain) {
        String[] strArr = new String[0];
        List<String> pars = new ArrayList<>(3 + retain.length * 2);

        pars.add("-x");
        pars.add("-f");
        pars.add("-d");
        pars.add("-f"); // Apparently this cleans directories containing .git folders.

        for (String pattern : retain) {
            pars.add("-e");
            pars.add(pattern);
        }

        Optional<ExecRes> clean = git.exec(dir, "clean", pars.toArray(strArr));
        Optional<ExecRes> reset = clean.filter(ExecRes::succeeded) // this is '&&' in bash
                                       .flatMap(r -> git.exec(dir, "reset", "--hard"));

        if (!reset.isPresent() || !reset.get().succeeded()) {
            LOG.warning("Failed to clean " + this);
            return false;
        }

        List<String> submoduleCleanPars = new ArrayList<>(8);
        submoduleCleanPars.add("foreach");
        submoduleCleanPars.add("--recursive");
        submoduleCleanPars.add(git.getCmd());
        submoduleCleanPars.add("clean");
        submoduleCleanPars.addAll(pars.subList(0, 4));

        List<String> submoduleResetPars = new ArrayList<>(5);
        submoduleResetPars.add("foreach");
        submoduleResetPars.add("--recursive");
        submoduleResetPars.add(git.getCmd());
        submoduleResetPars.add("reset");
        submoduleResetPars.add("--hard");

        Optional<ExecRes> cleanSub = git.exec(dir, "submodule", submoduleCleanPars.toArray(strArr));
        Optional<ExecRes> resetSub = cleanSub.filter(ExecRes::succeeded) // this is '&&' in bash
                                             .flatMap(r -> git.exec(dir, "submodule", submoduleResetPars.toArray(strArr)));

        if (!resetSub.isPresent() || !resetSub.get().succeeded()) {
            LOG.warning("Failed to clean submodules of " + this);
            return false;
        }

        return true;
    }

    /**
     * Archives the current state of the {@link Repository} in the given (ZIP) archive file. The file must not exist
     * when this method is called. The .zip extension will be added if not present. Necessary parent directories will
     * be created if necessary. This will only archive files under version control.
     *
     * @param archivePath the file to archive the current state of the {@link Repository} in
     * @return {@code true} iff the repository was successfully archived
     */
    public boolean archive(Path archivePath) {
        String zipSuffix = ".zip";
        String fileName = archivePath.getFileName().toString();

        if (!fileName.endsWith(zipSuffix)) {
            archivePath = archivePath.getParent().resolve(fileName + zipSuffix);
        }

        // To make it effectively final...
        Path actualPath = archivePath;

        if (Files.exists(actualPath)) {
            LOG.log(Level.SEVERE, () -> "Path " + actualPath + " already exists.");
            return false;
        }

        try {
            FileUtils.forceMkdirParent(actualPath.toFile());
        } catch (IOException e) {
            LOG.log(Level.SEVERE, e, () -> "Failed to make the parent directories for " + actualPath);
            return false;
        }

        LOG.fine("Archiving " + this + " in " + actualPath);

        // Get the paths that were under version control in HEAD
        Optional<Set<Path>> optPaths = git.exec(dir, "ls-tree", "-r", "HEAD", "--full-name", "--name-only")
                                             .filter(ExecRes::succeeded)
                                             .map(res -> {
                                                 String[] lines = res.getStdOutTrimmed().split("\\r?\\n");
                                                 return Stream.of(lines).map(Paths::get)
                                                              .collect(Collectors.toCollection(LinkedHashSet::new));
                                             });

        // Handle changed files (e.g. after a merge that added / removed files or produced conflicts)
        Optional<Status> optStatus = getStatus(false, false);

        if (optPaths.isEmpty() || optStatus.isEmpty()) {
            LOG.severe("Failed to get the files to archive.");
            return false;
        }

        Path repoDirPath = dir.toPath();
        Set<Path> archivePaths = optPaths.get();

        Status status = optStatus.get();
        Set<Path> changedPaths = new LinkedHashSet<>(status.changed.keySet());
        changedPaths.addAll(status.unmerged.keySet());

        for (Path changedPath : changedPaths) {
            // Add changed paths if they exist on disk
            if (Files.exists(repoDirPath.resolve(changedPath))) {
                if (archivePaths.add(changedPath)) {
                    LOG.finer("Adding uncommitted path '" + changedPath + "' to the archive.");
                }
            }
        }

        for (Iterator<Path> it = archivePaths.iterator(); it.hasNext(); ) {
            Path path = it.next();

            // Remove paths that do not exist (e.g. because they were deleted as part of an ongoing merge)
            if (Files.notExists(repoDirPath.resolve(path))) {
                LOG.finer("Removing non-existent path '" + path + "' from the archive.");
                it.remove();
            }
        }

        try (ZipOutputStream zipOut = new ZipOutputStream(new FileOutputStream(actualPath.toFile()), UTF_8)) {
            zipOut.setLevel(Deflater.BEST_COMPRESSION);

            for (Path path : archivePaths) {
                Path resolvedPath = repoDirPath.resolve(path);
                List<Path> pathsToZip;

                if (Files.isDirectory(resolvedPath)) {
                    pathsToZip = Files.walk(resolvedPath).filter(Files::isRegularFile).collect(Collectors.toList());
                } else if (Files.isRegularFile(resolvedPath)) {
                    pathsToZip = Collections.singletonList(path);
                } else {
                    LOG.warning("Skipping archival of invalid path " + path);
                    continue;
                }

                for (Path p : pathsToZip) {

                    String entryPath;

                    if (p.isAbsolute()) {
                        entryPath = repoDirPath.relativize(p).toString();
                    } else {
                        entryPath = p.toString();
                    }

                    ZipEntry entry = new ZipEntry(entryPath);
                    zipOut.putNextEntry(entry);
                    Files.copy(repoDirPath.resolve(p), zipOut);
                }
            }
        } catch (IOException e) {
            LOG.log(Level.SEVERE, "Failed to zip the files to archive.", e);
            return false;
        }

        return true;
    }

    /**
     * Returns the {@link GitWrapper} used by this {@link Repository}.
     *
     * @return the {@link GitWrapper}
     */
    public GitWrapper getGit() {
        return git;
    }

    /**
     * Returns the URL this {@link Repository} was cloned from.
     *
     * @return the URL
     */
    public String getUrl() {
        return url;
    }

    /**
     * Returns the directory containing this {@link Repository}.
     *
     * @return the repository directory
     */
    public File getDir() {
        return dir;
    }

    /**
     * Returns the name of the directory this {@link Repository} is based in.
     *
     * @return the name of the {@link Repository}
     */
    public String getName() {
        return getDir().getName();
    }

    @Override
    public String toString() {
        return String.format("Repository{url=%s, dir=%s}", url, dir);
    }

    @Override
    public final boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (!(o instanceof Repository)) {
            return false;
        }

        Repository that = (Repository) o;
        return Objects.equals(getUrl(), that.getUrl())
                && Objects.equals(getDir().getAbsoluteFile(), that.getDir().getAbsoluteFile());
    }

    @Override
    public final int hashCode() {
        return Objects.hash(getUrl(), getDir().getAbsoluteFile());
    }
}
