/**
 * Copyright (C) 2022 Georg Seibt
 *
 * This file is part of GitWrapper.
 *
 * GitWrapper is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GitWrapper is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with GitWrapper. If not, see <http://www.gnu.org/licenses/>.
 */
package de.uni_passau.fim.gitwrapper;

import de.uni_passau.fim.processexecutor.ProcessExecutor;

import java.util.Optional;
import java.util.function.Function;
import java.util.logging.Logger;

/**
 * A {@link Branch} is a named {@link Reference} to a git branch pointing to a {@link Commit}.
 */
public class Branch extends Reference {

    private static final Logger LOG = Logger.getLogger(Branch.class.getCanonicalName());

    // The prefixes of the full symbolic names of (remote) branches.
    static final String FULL_SYMBOLIC_BRANCH = "refs/heads/";
    static final String FULL_SYMBOLIC_REMOTE_BRANCH = "refs/remotes/";

    private boolean remote;

    /**
     * Constructs a new {@link Branch} referencing an existing git branch.
     *
     * @param repo   the repo this branch is part of
     * @param name   the branch name
     * @param remote whether this is a remote branch
     */
    Branch(Repository repo, String name, boolean remote) {
        super(repo, name);
        this.remote = remote;
    }

    /**
     * Checks this branch out, and tries to pull changes on this current branch.
     *
     * @return true if the pull was successful
     */
    public boolean pull() {
        if (!repo.checkout(this)) {
            return false;
        }

        Optional<ProcessExecutor.ExecRes> fetch = git.exec(repo.getDir(), "pull");
        Function<ProcessExecutor.ExecRes, Boolean> toBoolean = res -> {
            boolean failed = git.failed(res);

            if (failed) {
                LOG.warning(() -> String.format("Pull of %s failed.", this));
            }

            return !failed;
        };

        return fetch.map(toBoolean).orElse(false);
    }

    /**
     * Returns the {@link Commit} this {@link Branch} is pointing at or an empty optional if there is an error.
     *
     * @return optionally the tip of this {@link Branch}
     */
    public Optional<Commit> getTip() {
        return repo.toHash(id).map(repo::getCommitUnchecked);
    }

    /**
     * Returns the full name of this {@link Branch} e.g. 'refs/heads/master' or 'refs/remotes/origin/master'.
     *
     * @return the name of this {@link Branch}
     * @see #getName()
     */
    @Override
    public String getId() {
        return super.getId();
    }

    /**
     * Returns whether this is a remote {@link Branch}.
     *
     * @return true iff this {@link Branch} is a remote branch
     */
    public boolean isRemote() {
        return remote;
    }

    /**
     * Returns the short name of this {@link Branch} e.g. 'master' for 'refs/heads/master' or 'origin/master' for
     * 'refs/remotes/origin/master'.
     *
     * @return the name of the branch
     */
    public String getName() {
        String fullName = getId();

        if (remote && fullName.startsWith(FULL_SYMBOLIC_REMOTE_BRANCH)) {
            return fullName.substring(FULL_SYMBOLIC_REMOTE_BRANCH.length());
        } else if (!remote && fullName.startsWith(FULL_SYMBOLIC_BRANCH)) {
            return fullName.substring(FULL_SYMBOLIC_BRANCH.length());
        } else {
            return fullName;
        }
    }
}
