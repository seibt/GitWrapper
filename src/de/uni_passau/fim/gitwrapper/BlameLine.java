/**
 * Copyright (C) 2022 Georg Seibt
 *
 * This file is part of GitWrapper.
 *
 * GitWrapper is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GitWrapper is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with GitWrapper. If not, see <http://www.gnu.org/licenses/>.
 */
package de.uni_passau.fim.gitwrapper;

import java.nio.file.Path;
import java.util.Collections;
import java.util.Map;

/**
 * A line from a file with the associated 'git blame' information.
 */
public class BlameLine {

    /**
     * The last commit to change the line.
     */
    public final Commit commit;

    /**
     * The line number of the line in the original file.
     */
    public final int originalLineNumber;

    /**
     * The line number of the line in the final file.
     */
    public final int finalLineNumber;

    /**
     * The file in the commit that the line is attributed to.
     */
    public final Path file;

    /**
     * The actual line from the file.
     */
    public final String line;

    /**
     * Other (unrecognized) fields from the 'git blame' commit header. Unmodifiable.
     */
    public final Map<String, String> otherHeaderFields;

    /**
     * Constructs a new {@link BlameLine}.
     *
     * @param commit
     *         the last commit to change the line
     * @param originalLineNumber
     *         the line number of the line in the original file
     * @param finalLineNumber
     *         the line number of the line in the final file
     * @param file
     *         the file in the commit that the line is attributed to
     * @param line
     *         the actual line from the file
     * @param otherHeaderFields
     *         other (unrecognized) fields from the 'git blame' commit header
     */
    private BlameLine(Commit commit, int originalLineNumber, int finalLineNumber, Path file, String line,
                      Map<String, String> otherHeaderFields) {

        this.commit = commit;
        this.originalLineNumber = originalLineNumber;
        this.finalLineNumber = finalLineNumber;
        this.file = file;
        this.line = line;
        this.otherHeaderFields = otherHeaderFields;
    }

    /**
     * Constructs a new {@link BlameLine}. This method is to be used when the 'git blame' parser in
     * {@link Repository#parseBlameLines(String)} first reads the commit header for a commit.
     *
     * @param commit
     *         the last commit to change the line
     * @param originalLineNumber
     *         the line number of the line in the original file
     * @param finalLineNumber
     *         the line number of the line in the final file
     * @param file
     *         the file in the commit that the line is attributed to
     * @param line
     *         the actual line from the file
     * @param otherHeaderFields
     *         other (unrecognized) fields from the 'git blame' commit header
     * @return a {@link BlameLine} containing the given header fields
     */
    static BlameLine firstOccurrence(Commit commit, int originalLineNumber, int finalLineNumber, Path file, String line,
                                     Map<String, String> otherHeaderFields) {

        Map<String, String> other;

        if (otherHeaderFields.isEmpty()) {
            other = Collections.emptyMap();
        } else {
            other = Collections.unmodifiableMap(otherHeaderFields);
        }

        return new BlameLine(commit, originalLineNumber, finalLineNumber, file, line, other);
    }

    /**
     * Constructs a new {@link BlameLine}. This method is to be used when the 'git blame' parser encounters a line
     * attributed to a commit whose header was previously read an packaged in a {@link BlameLine}. References to the
     * header fields that are not line specific will be taken from {@code firstOccurrence} and stored in the returned
     * {@link BlameLine}.
     *
     * @param firstOccurrence
     *         the {@link BlameLine} representing the first occurrence of the commit it contains
     * @param originalLineNumber
     *         the line number of the line in the original file
     * @param finalLineNumber
     *         the line number of the line in the final file
     * @param line
     *         the actual line from the file
     * @return a {@link BlameLine} containing the given header fields and references to the {@code firstOccurrence}
     */
    static BlameLine nextOccurrence(BlameLine firstOccurrence, int originalLineNumber, int finalLineNumber,
                                    String line) {

        Commit commit = firstOccurrence.commit;
        Path file = firstOccurrence.file;
        Map<String, String> otherHeaderFields = firstOccurrence.otherHeaderFields;

        return new BlameLine(commit, originalLineNumber, finalLineNumber, file, line, otherHeaderFields);
    }
}
