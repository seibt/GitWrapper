/**
 * Copyright (C) 2022 Georg Seibt
 *
 * This file is part of GitWrapper.
 *
 * GitWrapper is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GitWrapper is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with GitWrapper. If not, see <http://www.gnu.org/licenses/>.
 */
package de.uni_passau.fim.gitwrapper;

import java.util.Optional;

@FunctionalInterface
public interface MergeStrategy {

    /**
     * Implements the merge between the {@link Reference References} {@code left} and {@code right}.
     *
     * @param left
     *         the left parent of the merge
     * @param right
     *         the right parent of the merge
     * @return optionally the number of files with merge conflicts after the merge, if there was no general error while
     *         merging
     */
    Optional<Integer> merge(Reference left, Reference right);
}
