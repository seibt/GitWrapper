/**
 * Copyright (C) 2022 Georg Seibt
 *
 * This file is part of GitWrapper.
 *
 * GitWrapper is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GitWrapper is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with GitWrapper. If not, see <http://www.gnu.org/licenses/>.
 */
package de.uni_passau.fim.gitwrapper;

import de.uni_passau.fim.processexecutor.ProcessExecutor;

import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * A git object referencing a tree-ish object.
 *
 * @see Commit
 * @see Branch
 */
public abstract class Reference {

    private static final Logger LOG = Logger.getLogger(Reference.class.getCanonicalName());

    protected final String id;
    protected final Repository repo;
    protected final GitWrapper git;

    /**
     * Constructs a new {@link Reference} belonging to the given {@code repo}.
     *
     * @param repo
     *         the {@link Repository} this {@link Reference} belongs to
     * @param id
     *         the ID of the {@link Reference}
     */
    protected Reference(Repository repo, String id) {
        this.id = id;
        this.repo = repo;
        this.git = repo.getGit();
    }

    /**
     * Returns the id of this reference. This can be a SHA1 hash or a reference name, but must resolve to a
     * valid git object.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Returns the {@link Repository} that this {@link Reference} belongs to.
     *
     * @return the {@link Repository} this {@link Reference} belongs to
     */
    public Repository getRepo() {
        return repo;
    }

    /**
     * Optionally returns the merge base for <code>this</code> and <code>other</code>. The <code>other</code> reference
     * must be part of the same {@link Repository} this {@link Reference} is.
     *
     * @param other
     *         the other {@link Reference}
     * @return the merge base or an empty {@link Optional} if there is no merge base or an exception occurred
     */
    public Optional<Commit> getMergeBase(Reference other) {
        if (!repo.equals(other.repo)) {
            LOG.warning(() -> {
                String msg = "Failed to obtain a merge base for %s and %s as they are not from the same repository.";
                return String.format(msg, this, other);
            });

            return Optional.empty();
        }

        Optional<ProcessExecutor.ExecRes> mergeBase = git.exec(repo.getDir(), "merge-base", getId(), other.getId());
        Function<ProcessExecutor.ExecRes, Commit> toCommit = res -> {

            if (git.failed(res)) {
                LOG.warning(() -> String.format("Failed to obtain a merge base for %s and %s.", this, other));
                return null;
            }

            Commit base = repo.getCommitUnchecked(res.getStdOutTrimmed());

            LOG.fine(() -> String.format("Commits %s and %s have the merge base %s.", this, other, base));

            return base;
        };

        return mergeBase.map(toCommit);
    }

    /**
     * Performs a forceful checkout of {@code this} {@link Reference} followed by a merge of <code>this</code> and
     * <code>other</code>. <code>Other</code> must be part of the same {@link Repository} this {@link Reference} is.
     *
     * @param other    the {@link Reference} to merge
     * @param strategy the {@link MergeStrategy} to use for merging
     * @return optionally the number of merge conflicts after the merge, if the merge was successful
     */
    public Optional<Integer> merge(Reference other, MergeStrategy strategy) {
        if (!repo.forceCheckout(this)) {
            return Optional.empty();
        }

        return strategy.merge(this, other);
    }

    /**
     * Returns the parents of this {@link Commit}.
     *
     * @return the parent {@link Commit} objects
     */
    public Optional<List<Commit>> getParents() {
        Optional<ProcessExecutor.ExecRes> revList = git.exec(repo.getDir(), "rev-list", "--parents", "-n", "1", id);
        Function<ProcessExecutor.ExecRes, List<Commit>> toParentsList = res -> {
            if (git.failed(res)) {
                LOG.warning(() -> String.format("Could not find the parents of %s.", this));
                return null;
            }

            String[] ids = res.getStdOutTrimmed().split("\\s+");

            LOG.fine(() -> String.format("Found %d parents for %s.", ids.length - 1, this));
            LOG.finer(() -> String.format("Commit id and parents are:%n%s", String.join(System.lineSeparator(), ids)));

            return Arrays.stream(ids).skip(1).map(repo::getCommitUnchecked).collect(Collectors.toList());
        };

        return revList.map(toParentsList);
    }

    /**
     * Checks whether this {@link Reference} contains the specified file.
     *
     * @param file
     *          the (repository relative) path to the file to look for in this {@link Reference}
     * @return {@code true} iff this {@link Reference} contains the file
     */
    public Optional<Boolean> hasFile(Path file) {
        Optional<ProcessExecutor.ExecRes> result = git.exec(repo.getDir(), "cat-file", "-e", id + ":" + file);
        return result.map(ProcessExecutor.ExecRes::succeeded);
    }

    @Override
    public final boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Reference ref = (Reference) o;
        return Objects.equals(id, ref.id) && Objects.equals(repo, ref.repo);
    }

    @Override
    public final int hashCode() {
        return Objects.hash(id, repo);
    }

    @Override
    public String toString() {
        return id;
    }
}
