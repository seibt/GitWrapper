/**
 * Copyright (C) 2022 Georg Seibt
 *
 * This file is part of GitWrapper.
 *
 * GitWrapper is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GitWrapper is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with GitWrapper. If not, see <http://www.gnu.org/licenses/>.
 */
package de.uni_passau.fim.gitwrapper;

import java.nio.file.Path;
import java.util.Collections;
import java.util.List;

/**
 * A merge conflict parsed from the output of 'git blame' by {@link Repository#blameUnmergedFile(Path)} (String)}.
 */
public class MergeConflict {

    /**
     * The lines making up the left side of the conflict. Unmodifiable.
     */
    public final List<BlameLine> left;

    /**
     * The lines making up the right side of the conflict. Unmodifiable.
     */
    public final List<BlameLine> right;

    /**
     * Constructs a new {@link MergeConflict}. The given lists {@code left} and {@code right} will be stored
     * unmodifiable.
     *
     * @param left
     *         the left lines of the conflict
     * @param right
     *         the right lines of the conflict
     */
    MergeConflict(List<BlameLine> left, List<BlameLine> right) {
        this.left = Collections.unmodifiableList(left);
        this.right = Collections.unmodifiableList(right);
    }
}
