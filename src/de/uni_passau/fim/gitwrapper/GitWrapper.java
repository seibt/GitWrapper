/**
 * Copyright (C) 2022 Georg Seibt
 *
 * This file is part of GitWrapper.
 *
 * GitWrapper is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GitWrapper is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with GitWrapper. If not, see <http://www.gnu.org/licenses/>.
 */
package de.uni_passau.fim.gitwrapper;

import de.uni_passau.fim.processexecutor.ProcessExecutor;
import de.uni_passau.fim.processexecutor.ToolNotWorkingException;
import de.uni_passau.fim.processexecutor.ToolWrapper;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.Collections.singletonList;

/**
 * A wrapper for executing git commands.
 */
public class GitWrapper extends ToolWrapper {

    private static final Logger LOG = Logger.getLogger(GitWrapper.class.getCanonicalName());

    private static final String FATAL_PREFIX = "fatal";
    private static final String ERROR_PREFIX = "error";

    private static final Pattern CLONING_INTO = Pattern.compile("Cloning into '(.*)'\\.\\.\\.");
    private static final Pattern ALREADY_EXISTS = Pattern.compile("fatal: destination path '(.*)' already exists and is not an empty directory\\.");

    private static final String GIT_VERSION_C = "--version";
    private static final String GIT_VERSION_PREFIX = "git version";

    private static final Map<String, String> defaultEnvironment;
    private static final List<String> defaultOptions;

    static {
        Map<String, String> env = new HashMap<>();
        String locale = "C";

        env.put("LANGUAGE", locale);
        env.put("LC_ALL", locale);
        env.put("LANG", locale);
        defaultEnvironment = Collections.unmodifiableMap(env);

        List<String> defOptions = new ArrayList<>();

        defOptions.add("-c");
        defOptions.add("core.quotePath=false");
        defOptions.add("-c");
        defOptions.add("core.precomposeUnicode=true");

        // merges resulting in commits fail, if no committer is set
        defOptions.add("-c");
        defOptions.add("user.name=gitwrapper");
        defOptions.add("-c");
        defOptions.add("user.email=gitwrapper@example.com");
        defaultOptions = Collections.unmodifiableList(defOptions);
    }

    /**
     * Constructs a new {@link GitWrapper}
     *
     * @param git
     *         the git command, e.g. '/usr/bin/git' or just 'git'
     * @throws ToolNotWorkingException
     *         if the supplied {@code git} command is not working
     */
    public GitWrapper(String git) throws ToolNotWorkingException {
        super(git);
    }

    @Override
    protected boolean isWorking() {
        Function<ProcessExecutor.ExecRes, Boolean> checkPrefix = execRes -> execRes.getStdOutTrimmed().startsWith(GIT_VERSION_PREFIX);
        return exec(new File("."), true, singletonList(GIT_VERSION_C)).map(checkPrefix).orElse(false);
    }

    /**
     * Attempts to clone the git repository at the given <code>url</code> into a directory under <code>parentDir</code>.
     * If <code>parentDir</code> already contains a git repository with the name that would be produced when cloning
     * <code>url</code> it is assumed to be a clone of <code>url</code> and {@link #importRepository(File) imported} as a {@link Repository}.
     *
     * @param parentDir
     *         the directory in which to place the clone of <code>url</code>
     * @param url
     *         the git repository to clone
     * @param fetch
     *         whether to perform a fetch of the {@link Repository} if a clone of it was already found on disk
     * @return the resulting {@link Repository} or an empty {@link Optional} if there is an error
     */
    public Optional<Repository> clone(File parentDir, String url, boolean fetch) {
        LOG.fine(() -> String.format("Cloning '%s'.", url));

        Optional<ProcessExecutor.ExecRes> res = exec(parentDir, "clone", "--recurse-submodules", url);
        Function<ProcessExecutor.ExecRes, Repository> toRepo = execRes -> {
            Scanner sc = new Scanner(execRes.getStdOutTrimmed());

            if (!sc.hasNextLine()) {
                LOG.warning(() -> String.format("No output while cloning '%s'.", url));
                return null;
            }

            String firstLine = sc.nextLine();
            Matcher matcher;

            if (execRes.succeeded()) {
                matcher = CLONING_INTO.matcher(firstLine);
            } else {
                matcher = ALREADY_EXISTS.matcher(firstLine);
            }

            if (!matcher.find()) {
                LOG.warning(() -> String.format("Unexpected output while cloning '%s'.", url));
                return null;
            }

            String name = matcher.group(1);
            File repoDir = new File(parentDir, name);
            String repoPath = repoDir.getAbsolutePath();

            if (execRes.succeeded()) {
                LOG.fine(() -> String.format("Cloned '%s' into '%s'.", url, repoPath));
                return new Repository(this, url, repoDir);
            } else {
                LOG.fine(() -> String.format("Found '%s' which would be produced for '%s'", repoPath, url));

                if (!isGitDir(repoDir)) {
                    LOG.warning(() -> String.format("'%s' is not a git repository.", repoPath));
                    return null;
                }

                LOG.fine(() -> String.format("'%s' is a git repository. Assuming it is a clone of '%s'.", repoPath, url));
                Optional<Repository> maybeRepo = importRepository(repoDir);

                if (!maybeRepo.isPresent()) {
                    LOG.warning("Could not import repo from " + repoDir);
                    return null;
                }
                Repository repo = maybeRepo.get();

                if (!repo.getUrl().equals(url)) {
                    LOG.warning("URL of imported repository (" + repo.getUrl() + ") does not match given URL (" + url + "). Proceeding anyway.");
                }

                if (fetch && !repo.fetch()) {
                    LOG.warning("Could not fetch " + repo + ". It may be out of date.");
                }

                return repo;
            }
        };

        return res.map(toRepo);
    }

    /**
     * Imports an existing repository.
     *
     * @param directory
     *         the path to the repository
     * @return the {@link Repository} if the import was successful or an empty {@link Optional} if there was an error
     */
    public Optional<Repository> importRepository(File directory) {
        if (!isGitDir(directory)) {
            LOG.warning(() -> directory + " does not contain a git repository.");
            return Optional.empty();
        }

        Optional<ProcessExecutor.ExecRes> originURL = exec(directory, "config", "--get", "remote.origin.url");
        Function<ProcessExecutor.ExecRes, Repository> toRepository = res -> {
            String repoUrl = null;

            if (failed(res)) {
                LOG.warning(() -> String.format("Failed to get the origin url for the repository in %s. Using the " +
                                                "canonical directory path.", directory));
            } else if (res.getStdOutTrimmed().isEmpty()) {
                LOG.warning(() -> String.format("Repository in %s does not have an origin url. Using the canonical " +
                                                "directory path.", directory));
            } else {
                repoUrl = res.getStdOutTrimmed();
            }

            if (repoUrl == null) {

                try {
                    repoUrl = directory.getCanonicalPath();
                } catch (IOException e) {
                    LOG.log(Level.WARNING, e, () -> "Exception computing the canonical path for " + directory +
                                                    ". Using the absolute path instead.");
                    repoUrl = directory.getAbsolutePath();
                }
            }

            return new Repository(this, repoUrl, directory);
        };

        return originURL.map(toRepository);
    }

    /**
     * Returns whether the given directory (or any of its parent directories) is a git repository. Returns false if
     * <code>directory</code> is not a directory.
     *
     * @param directory
     *         the directory to check
     * @return true iff the given directory (or any of its parent directories) is a git repository
     */
    public boolean isGitDir(File directory) {

        if (!directory.isDirectory()) {
            LOG.warning(() -> String.format("The file '%s' does not exist or is not a directory.", directory));
            return false;
        }

        Optional<ProcessExecutor.ExecRes> status = exec(directory, "rev-parse", "--is-inside-git-dir");
        return status.map(res -> !failed(res)).orElse(false);
    }

    /**
     * Returns whether the given git command failed. This relies on exit code first and then on the assumption that
     * the output (in case of failure) starts with either "{@value FATAL_PREFIX}" or "{@value ERROR_PREFIX}".
     *
     * @param res
     *         the {@link ProcessExecutor.ExecRes} to check
     * @return whether the git command failed
     */
    public boolean failed(ProcessExecutor.ExecRes res) {
        return res.failed() || failedPrefix(res);
    }

    /**
     * Returns whether the given git command failed by checking whether the output starts with either
     * "{@value FATAL_PREFIX}" or "{@value ERROR_PREFIX}". This method ignores the exit code.
     *
     * @param res
     *         the {@link ProcessExecutor.ExecRes} to check
     * @return whether the git command failed
     */
    public boolean failedPrefix(ProcessExecutor.ExecRes res) {
        return res.getStdOutTrimmed().startsWith(FATAL_PREFIX) || res.getStdOutTrimmed().startsWith(ERROR_PREFIX);
    }

    /**
     * Converts the given {@code path} {@link String} to a {@link Path} object. This method will perform unquoting if
     * necessary. If there is an exception unquoting or converting to a path an empty optional will be returned.
     *
     * @param path
     *         the {@link String} to convert to a {@link Path}
     * @return optionally the {@link Path} corresponding to the given {@link String} returned by git
     */
    public Optional<Path> getPath(String path) {
        Objects.requireNonNull(path, "The given String 'path' must not be null.");

        if (path.length() >= 2 && path.charAt(0) == '"' && path.charAt(path.length() - 1) == '"') {

            try {
                path = unquote(path);
            } catch (IllegalArgumentException e) {
                LOG.log(Level.WARNING, "Failed to unquote the path '" + path + "'.", e);
                return Optional.empty();
            }
        }

        try {
            return Optional.of(Paths.get(path));
        } catch (InvalidPathException e) {
            LOG.log(Level.WARNING, "Failed to construct a Path object from path '" + path + "'.", e);
            return Optional.empty();
        }
    }

    /**
     * Unquotes the given {@link String}. This method assumes that the given {@link String} begins and ends with the
     * character '"'. It will replace escape sequences \a, \b, \f, \n, \r, \t, \v, \\, and \" with their corresponding
     * unquoted characters.
     *
     * @param quoted
     *         the quoted {@link String}
     * @return the unquoted {@link String}
     * @throws IllegalArgumentException
     *         if the given {@link String} does not start with a '"' character or there is an
     *         illegal escape sequence in {@code quoted}
     */
    private String unquote(String quoted) {
        StringReader rdr = new StringReader(quoted);
        StringWriter wtr = new StringWriter(quoted.length() - 2);

        int ch;

        try {
            if (rdr.read() != '"') {
                throw new IllegalArgumentException("Quoted strings begin with the character <\">");
            }

            while ((ch = rdr.read()) != -1) {

                if (ch == '"') {
                    break;
                } else if (ch != '\\') {
                    wtr.write(ch);
                    continue;
                }

                switch ((ch = rdr.read())) {
                    case 'a':
                        wtr.write(0x07);
                        break;
                    case 'b':
                        wtr.write('\b');
                        break;
                    case 'f':
                        wtr.write('\f');
                        break;
                    case 'n':
                        wtr.write('\n');
                        break;
                    case 'r':
                        wtr.write('\r');
                        break;
                    case 't':
                        wtr.write('\t');
                        break;
                    case 'v':
                        wtr.write(0x0B);
                        break;
                    case '\\': case '"':
                        wtr.write(ch);
                        break;
                    default:
                        throw new IllegalArgumentException("Illegal escape character <" + (char) ch + ">.");
                }
            }
        } catch (IOException e) {
            throw new RuntimeException("The StringReader will not be closed and as such can not throw an IOException.", e);
        }

        return wtr.toString();
    }

    /**
     * Executes '&lt;git command&gt; &lt;firstParameter&gt; &lt;parameters&gt;' and returns the exit code and the output
     * of the command. If there is an exception executing the command an empty {@link Optional} will be returned.
     *
     * @param workingDirectory
     *         the working directory for the command execution
     * @param firstParameter
     *         the first git parameter
     * @param parameters
     *         the git parameters
     * @return the result of the execution
     */
    public Optional<ProcessExecutor.ExecRes> exec(File workingDirectory, String firstParameter, String... parameters) {
        return exec(workingDirectory, null, firstParameter, parameters);
    }

    /**
     * Executes '&lt;git command&gt; &lt;firstParameter&gt; &lt;parameters&gt;' and returns the exit code and the output
     * of the command. If there is an exception executing the command an empty {@link Optional} will be returned.
     *
     * @param workingDirectory
     *         the working directory for the command execution
     * @param environment
     *         additional environment variables for the command execution
     * @param firstParameter
     *         the first git parameter
     * @param parameters
     *         the git parameters
     * @return the result of the execution
     */
    public Optional<ProcessExecutor.ExecRes> exec(File workingDirectory, Map<String, String> environment, String firstParameter, String... parameters) {
        List<String> pars = new ArrayList<>(defaultOptions.size() + ((parameters != null) ? parameters.length + 1 : 1));

        pars.addAll(defaultOptions);
        pars.add(firstParameter);

        if (parameters != null) {
            Collections.addAll(pars, parameters);
        }

        Map<String, String> env;

        if (environment != null) {
            env = new HashMap<>(defaultEnvironment.size() + environment.size());
            env.putAll(environment);
            env.putAll(defaultEnvironment);
        } else {
            env = GitWrapper.defaultEnvironment;
        }

        return exec(workingDirectory, true, env, pars);
    }
}
