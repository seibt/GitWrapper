/**
 * Copyright (C) 2022 Georg Seibt
 *
 * This file is part of GitWrapper.
 *
 * GitWrapper is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GitWrapper is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with GitWrapper. If not, see <http://www.gnu.org/licenses/>.
 */
package de.uni_passau.fim.processexecutor;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.logging.Level.FINER;
import static java.util.logging.Level.WARNING;

/**
 * Contains static methods for executing commands and gathering their output and exit code.
 */
public class ProcessExecutor {

    private static final Logger LOG = Logger.getLogger(ProcessExecutor.class.getCanonicalName());

    private static final ExecutorService exService = Executors.newCachedThreadPool(r -> {
        Thread t = Executors.defaultThreadFactory().newThread(r);

        t.setDaemon(true);
        return t;
    });

    /**
     * The result of a process execution.
     */
    public static final class ExecRes {

        public static final int EXIT_SUCCESS = 0;

        /**
         * The exit code of the process that produced this {@link ExecRes}.
         */
        public final int exitCode;

        /**
         * The full standard output of the process that produced this {@link ExecRes}.
         */
        public final String stdOut;

        /**
         * The full standard error output of the process that produced this {@link ExecRes}.
         */
        public final String stdErr;

        private String stdOutTrimmed;
        private String stdErrTrimmed;

        /**
         * Constructs a new {@link ExecRes} containing the given results from the execution of a process.
         *
         * @param exitCode
         *         the exit code of the process
         * @param stdOut
         *         the standard output of the process
         * @param stdErr
         *         the standard error output of the process
         */
        private ExecRes(int exitCode, String stdOut, String stdErr) {
            this.exitCode = exitCode;
            this.stdOut = stdOut;
            this.stdErr = stdErr;
        }

        /**
         * Returns a trimmed version of the standard output contained in this {@link ExecRes}.
         *
         * @return the trimmed standard output
         */
        public String getStdOutTrimmed() {

            if (stdOutTrimmed == null) {
                stdOutTrimmed = stdOut.trim();
            }

            return stdOutTrimmed;
        }

        /**
         * Returns a trimmed version of the standard error output contained in this {@link ExecRes}.
         *
         * @return the trimmed standard error output
         */
        public String getStdErrTrimmed() {

            if (stdErrTrimmed == null) {
                stdErrTrimmed = stdErr.trim();
            }

            return stdErrTrimmed;
        }

        /**
         * Returns whether the exit code contained in this {@link ExecRes} is {@value ExecRes#EXIT_SUCCESS}.
         *
         * @return whether the exit code in this {@link ExecRes} indicates successful execution
         */
        public boolean succeeded() {
            return exitCode == EXIT_SUCCESS;
        }

        /**
         * Returns whether the exit code contained in this {@link ExecRes} is anything other than
         * {@value ExecRes#EXIT_SUCCESS}.
         *
         * @return whether the exit code in this {@link ExecRes} indicates a failed execution
         */
        public boolean failed() {
            return exitCode != EXIT_SUCCESS;
        }
    }

    /**
     * Executes the given <code>command</code>. If <code>command</code> is <code>null</code> an empty {@link Optional}
     * will be returned.
     *
     * @param workingDir
     *         the working directory for the execution
     * @param redirectError
     *         whether to merge the standard output and error streams
     * @param command
     *         the command to be executed
     * @return the result of the execution or an empty {@link Optional} if there is an exception executing the command
     */
    public static Optional<ExecRes> exec(File workingDir, boolean redirectError, List<String> command) {
        return exec(workingDir, redirectError, Collections.emptyMap(), command);
    }

    /**
     * Executes the given <code>command</code>. If <code>command</code> or <code>env</code> is <code>null</code> an
     * empty {@link Optional} will be returned.
     *
     * @param workingDir
     *         the working directory for the execution
     * @param redirectError
     *         whether to merge the standard output and error streams
     * @param env
     *         environment variables to be added to the ones inherited from the JVM
     * @param command
     *         the command to be executed
     * @return the result of the execution or an empty {@link Optional} if there is an exception executing the command
     */
    public static Optional<ExecRes> exec(File workingDir, boolean redirectError, Map<String, String> env,
                                         List<String> command) {

        if (command == null || env == null) {
            return Optional.empty();
        }

        ProcessBuilder builder = new ProcessBuilder();

        builder.command(command);
        builder.directory(workingDir);
        builder.redirectErrorStream(redirectError);

        try {
            builder.environment().putAll(env);
        } catch (UnsupportedOperationException | IllegalArgumentException e) {
            LOG.log(WARNING, () -> {
                String cmd = String.join(" ", builder.command());
                return String.format("Failed to set up the environment for the execution of '%s'", cmd);
            });

            return Optional.empty();
        }

        return exec(builder);
    }

    /**
     * Executes the command represented by the given <code>builder</code>.
     *
     * @param builder
     *         the {@link ProcessBuilder} to be executed
     * @return the result of the execution or an empty {@link Optional} if there is an exception executing the command
     */
    public static Optional<ExecRes> exec(ProcessBuilder builder) {
        String cmd = String.join(" ", builder.command());
        LOG.fine(() -> String.format("Executing '%s' in '%s'.", cmd, builder.directory().getAbsolutePath()));

        try {
            Process p = builder.start();

            String stdOut;
            String stdErr;

            if (builder.redirectErrorStream()) {
                stdOut = IOUtils.toString(p.getInputStream(), UTF_8);
                stdErr = IOUtils.toString(p.getErrorStream(), UTF_8);
            } else {
                Future<String> outF = exService.submit(() -> IOUtils.toString(p.getInputStream(), UTF_8));
                Future<String> errF = exService.submit(() -> IOUtils.toString(p.getErrorStream(), UTF_8));

                try {
                    stdOut = outF.get();
                    stdErr = errF.get();
                } catch (InterruptedException e) {
                    LOG.log(WARNING, e, () -> String.format("Interrupted while waiting for '%s' to finish.", cmd));

                    outF.cancel(true);
                    errF.cancel(true);
                    p.destroyForcibly();
                    return Optional.empty();
                } catch (ExecutionException e) {
                    LOG.log(WARNING, e, () -> String.format("Exception while reading the output of '%s'.", cmd));

                    outF.cancel(true);
                    errF.cancel(true);
                    p.destroyForcibly();
                    return Optional.empty();
                }
            }

            try { p.getInputStream().close(); } catch (IOException ignored) { }
            try { p.getErrorStream().close(); } catch (IOException ignored) { }
            try { p.getOutputStream().close(); } catch (IOException ignored) { }

            int exitCode;

            try {
                exitCode = p.waitFor();
            } catch (InterruptedException e) {
                LOG.log(WARNING, e, () -> String.format("Interrupted while waiting for '%s' to finish.", cmd));

                p.destroyForcibly();
                return Optional.empty();
            }

            ExecRes res = new ExecRes(exitCode, stdOut, stdErr);

            LOG.fine(() -> String.format("Execution of '%s' returned exit code %d.", cmd, res.exitCode));

            if (LOG.isLoggable(FINER) && res.getStdOutTrimmed().isEmpty()) {
                LOG.finer(() -> String.format("Execution of '%s' returned no standard output.", cmd));
            } else if (LOG.isLoggable(FINER)) {
                LOG.finer(() -> String.format("Execution of '%s' returned standard output.", cmd));
                LOG.finest(() -> String.format("Standard output of '%s' was:%n%s", cmd, res.getStdOutTrimmed()));
            }

            if (LOG.isLoggable(FINER) && res.getStdErrTrimmed().isEmpty()) {
                LOG.finer(() -> String.format("Execution of '%s' returned no standard error output.", cmd));
            } else if (LOG.isLoggable(FINER)) {
                LOG.finer(() -> String.format("Execution of '%s' returned standard error output.", cmd));
                LOG.finest(() -> String.format("Standard error output of '%s' was:%n%s", cmd, res.getStdErrTrimmed()));
            }

            return Optional.of(res);
        } catch (IOException e) {
            LOG.log(Level.SEVERE, e, () -> String.format("Exception executing '%s'.", cmd));
            return Optional.empty();
        }
    }
}
