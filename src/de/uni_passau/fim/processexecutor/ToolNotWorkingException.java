/**
 * Copyright (C) 2022 Georg Seibt
 *
 * This file is part of GitWrapper.
 *
 * GitWrapper is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GitWrapper is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with GitWrapper. If not, see <http://www.gnu.org/licenses/>.
 */
package de.uni_passau.fim.processexecutor;

/**
 * An {@link Exception} indicating that a wrapped tool is not working.
 */
public class ToolNotWorkingException extends Exception {

    private static final long serialVersionUID = 1L;

    public ToolNotWorkingException() {}

    public ToolNotWorkingException(String message) {
        super(message);
    }

    public ToolNotWorkingException(String message, Throwable cause) {
        super(message, cause);
    }

    public ToolNotWorkingException(Throwable cause) {
        super(cause);
    }

    public ToolNotWorkingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
