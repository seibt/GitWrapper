/**
 * Copyright (C) 2022 Georg Seibt
 *
 * This file is part of GitWrapper.
 *
 * GitWrapper is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GitWrapper is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with GitWrapper. If not, see <http://www.gnu.org/licenses/>.
 */
package de.uni_passau.fim.processexecutor;

import de.uni_passau.fim.processexecutor.ProcessExecutor.ExecRes;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Abstract superclass of all wrappers for external tools.
 */
public abstract class ToolWrapper {

    protected String cmd;

    /**
     * Constructs a new {@link ToolWrapper} wrapping the given external command <code>cmd</code>. Calls
     * {@link #isWorking()} and throws a {@link ToolNotWorkingException} if it returns <code>false</code>.
     *
     * @param cmd
     *         the external command to wrap
     * @throws ToolNotWorkingException
     *         if {@link #isWorking()} returns <code>false</code>
     */
    protected ToolWrapper(String cmd) throws ToolNotWorkingException {
        this.cmd = cmd;

        if (!isWorking()) {
            throw new ToolNotWorkingException(cmd + " is not working.");
        }
    }

    /**
     * Returns the command represented by this {@link ToolWrapper}.
     *
     * @return the command
     */
    public String getCmd() {
        return cmd;
    }

    /**
     * Determines whether the wrapped command is working as expected. This method is called by the
     * {@link #ToolWrapper(String)} constructor after the {@link #cmd} variable is initialized. Subclasses may use the
     * {@link #exec(File, boolean, List)} method in their implementation of this method.
     *
     * @return true iff the command is working
     */
    protected abstract boolean isWorking();

    /**
     * Prepends the wrapped external command to (a copy of) the <code>parameters</code> and calls
     * {@link ProcessExecutor#exec(File, boolean, List)}.
     *
     * @param workingDir
     *         the working directory for the execution
     * @param redirectError
     *         whether to merge the standard output and error streams
     * @param parameters
     *         the parameters for the external command
     * @return the result of the execution or an empty {@link Optional} if there is an exception executing the command
     */
    protected Optional<ExecRes> exec(File workingDir, boolean redirectError, List<String> parameters) {
        return ProcessExecutor.exec(workingDir, redirectError, addCommand(parameters));
    }

    /**
     * Prepends the wrapped external command to (a copy of) the <code>parameters</code> and calls
     * {@link ProcessExecutor#exec(File, boolean, Map, List)}.
     *
     * @param workingDir
     *         the working directory for the execution
     * @param redirectError
     *         whether to merge the standard output and error streams
     * @param env
     *         environment variables to be added to the ones inherited from the JVM
     * @param parameters
     *         the parameters for the external command
     * @return the result of the execution or an empty {@link Optional} if there is an exception executing the command
     */
    protected Optional<ExecRes> exec(File workingDir, boolean redirectError, Map<String, String> env, List<String> parameters) {
        return ProcessExecutor.exec(workingDir, redirectError, env, addCommand(parameters));
    }

    /**
     * Prepends the wrapped external command to (a copy of) the <code>parameters</code>.
     *
     * @param parameters
     *         the parameters for the external command
     * @return the command to be executed
     */
    private List<String> addCommand(List<String> parameters) {
        List<String> pars = new ArrayList<>(parameters.size() + 1);

        pars.add(cmd);
        pars.addAll(parameters);

        return pars;
    }
}
